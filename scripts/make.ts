import { promises as fs } from 'fs'
import pt from 'path'

type Item = {
	title: string
	clines: string[]
	tlines: string[]
	times: { real: string; user: string; sys: string }
}

const main = async () => {
	const txt = await fs.readFile(pt.join(__dirname, '../tmp.txt'), 'utf-8')
	let mode = 'find' as 'find' | 'find-start' | 'read-files' | 'read-time'
	let title = ''
	let clines: string[] = []
	let tlines: string[] = []
	let times = { real: '', user: '', sys: '' }
	const items: Item[] = []
	for (const line of txt.split(/\n/giu)) {
		switch (mode) {
			case 'find': {
				if (line.startsWith('## ')) {
					title = line
					mode = 'find-start'
				}
				break
			}
			case 'find-start': {
				if (/^Page\s+Size\s+First Load JS$/.test(line)) {
					mode = 'read-files'
					clines = [line]
					tlines = []
					times = { real: '', user: '', sys: '' }
				}
				break
			}
			case 'read-files': {
				if (line) {
					clines.push(line)
				} else {
					mode = 'read-time'
				}
				break
			}
			case 'read-time': {
				const [, k, v] = line.match(/^(real|user|sys)\s+([hms\d,\.]+)$/) || []
				if (('real' === k || 'user' === k || 'sys' === k) && v) {
					times[k] = v
					tlines.push(line)
				}
				if ('sys' === k) {
					items.push({
						title,
						clines,
						tlines,
						times,
					})
					mode = 'find'
				}
				break
			}
		}
	}
	await fs.writeFile(
		pt.join(__dirname, '../README.md'),
		`
https://github.com/privatenumber/esbuild-loader/issues/84

${items
	.map(
		item => `${item.title}

\`\`\`
${item.clines.join('\n')}

${item.tlines.join('\n')}
\`\`\`
`,
	)
	.join('\n')}

# まとめ

| - | size | real | user | sys
| :-- | :-- | :-- | :-- | :--
${items
	.map(item =>
		[
			item.title.slice(3),
			findSize(item),
			item.times.real,
			item.times.user,
			item.times.sys,
		]
			.map(t => `| ${t}`)
			.join(' '),
	)
	.join('\n')}
`,
	)
}

const findSize = (item: Item) => {
	const [, a, b] =
		item.clines
			.map(f => f.match(/^┌ ○ \/\s+([,\.\d]+ [kmg]*B)\s+([,\.\d]+ [kmg]*B)$/iu))
			.find(Boolean) || []
	return `${a} + ${b}`
}

main().catch(x => {
	console.error(x)
})
