
https://github.com/privatenumber/esbuild-loader/issues/84

## デフォルト

```
Page                                                           Size     First Load JS
┌ ○ /                                                          223 B          29.5 kB
└ ○ /404                                                       3.68 kB          33 kB
+ First Load JS shared by all                                  29.3 kB
  ├ chunks/f6078781a05fe1bcb0902d23dbbb2662c8d200b3.365021.js  13.3 kB
  ├ chunks/framework.e94ee7.js                                 7.88 kB
  ├ chunks/main.3ede60.js                                      6.37 kB
  ├ chunks/pages/_app.33ba4a.js                                1.01 kB
  └ chunks/webpack.50bee0.js                                   751 B

real	0m9.304s
user	0m20.549s
sys	0m1.944s
```

## next-babel-loader の代わりに esbuild-loader を使う

```
Page                                                           Size     First Load JS
┌ ○ /                                                          229 B            24 kB
└ ○ /404                                                       2.34 kB        26.1 kB
+ First Load JS shared by all                                  23.8 kB
  ├ chunks/f6078781a05fe1bcb0902d23dbbb2662c8d200b3.919938.js  8.76 kB
  ├ chunks/framework.e94ee7.js                                 7.88 kB
  ├ chunks/main.1b1d09.js                                      5.79 kB
  ├ chunks/pages/_app.5d50e9.js                                633 B
  └ chunks/webpack.50bee0.js                                   751 B

real	0m7.981s
user	0m18.861s
sys	0m1.924s
```

## terser の代わりに esbuild を使う

```
Page                                                           Size     First Load JS
┌ ○ /                                                          231 B          31.4 kB
└ ○ /404                                                       3.9 kB         35.1 kB
+ First Load JS shared by all                                  31.2 kB
  ├ chunks/f6078781a05fe1bcb0902d23dbbb2662c8d200b3.9b60e6.js  14.4 kB
  ├ chunks/framework.94f2e5.js                                 8.06 kB
  ├ chunks/main.50e19e.js                                      6.87 kB
  ├ chunks/pages/_app.a2e40b.js                                1.03 kB
  └ chunks/webpack.7d10d5.js                                   760 B

real	0m8.307s
user	0m14.529s
sys	0m1.722s
```

## next-babel-loader の代わりに esbuild-loader を terser の代わりに esbuild を使う

```
Page                                                           Size     First Load JS
┌ ○ /                                                          239 B          25.1 kB
└ ○ /404                                                       2.43 kB        27.3 kB
+ First Load JS shared by all                                  24.9 kB
  ├ chunks/f6078781a05fe1bcb0902d23dbbb2662c8d200b3.9141be.js  9.33 kB
  ├ chunks/framework.94f2e5.js                                 8.06 kB
  ├ chunks/main.00d93b.js                                      6.08 kB
  ├ chunks/pages/_app.160212.js                                646 B
  └ chunks/webpack.7d10d5.js                                   760 B

real	0m6.369s
user	0m11.854s
sys	0m1.662s
```


# まとめ

| - | size | real | user | sys
| :-- | :-- | :-- | :-- | :--
| デフォルト | 223 B + 29.5 kB | 0m9.304s | 0m20.549s | 0m1.944s
| next-babel-loader の代わりに esbuild-loader を使う | 229 B + 24 kB | 0m7.981s | 0m18.861s | 0m1.924s
| terser の代わりに esbuild を使う | 231 B + 31.4 kB | 0m8.307s | 0m14.529s | 0m1.722s
| next-babel-loader の代わりに esbuild-loader を terser の代わりに esbuild を使う | 239 B + 25.1 kB | 0m6.369s | 0m11.854s | 0m1.662s
