# bash ./testrun.sh 1>| tmp.txt 2>&1

set -Ceu

yarn rimraf out-def-def out-esbuild-def out-def-esbuild out-esbuild-esbuild
echo '## デフォルト'
time yarn build
mv out out-def-def
echo '## next-babel-loader の代わりに esbuild-loader を使う'
time APP_BUILD_MODE=ESBUILD yarn build
mv out out-esbuild-def
echo '## terser の代わりに esbuild を使う'
time APP_MINIFY_MODE=ESBUILD yarn build
mv out out-def-esbuild
echo '## next-babel-loader の代わりに esbuild-loader を terser の代わりに esbuild を使う'
time APP_BUILD_MODE=ESBUILD APP_MINIFY_MODE=ESBUILD yarn build
mv out out-esbuild-esbuild
