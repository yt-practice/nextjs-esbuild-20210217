/**
 * 下記を参考にしています
 * https://github.com/privatenumber/esbuild-loader/issues/84#issuecomment-756074727
 */

// const path = require('path')
const withPreact = require('next-plugin-preact')
const { ESBuildPlugin, ESBuildMinifyPlugin } = require('esbuild-loader')
const tsconfig = require('./tsconfig.json')

const basePath = ''
module.exports = withPreact({
	assetPrefix: basePath,
	// basePath,
	publicRuntimeConfig: {
		basePath,
	},
	experimental: {
		optimizeFonts: true,
		optimizeImages: true,
	},
	webpack(config, { webpack, dev }) {
		if ('ESBUILD' === process.env.APP_BUILD_MODE)
			if (!dev) {
				config.plugins.push(
					new webpack.ProvidePlugin({
						/**
						 *  Not sure why, This increases bundle size
						 * when you would think the oposite would be true!
						 */
						// __jsx: ['react', 'createElement'],
						// __fragment: ['react', 'Fragment'],
						React: 'react',
					}),
				)
				config.plugins.push(new ESBuildPlugin())
				const convertToESBuild = obj => {
					if ('next-babel-loader' === obj.loader) {
						return {
							loader: 'esbuild-loader',
							options: {
								loader: 'tsx',
								target: 'es2017',
								tsconfigRaw: tsconfig,
								// jsxFactory: '__jsx',
								// jsxFragment: '__fragment',
							},
						}
					}
					return obj
				}

				const rule = config.module.rules[0]
				if (rule) {
					if (Array.isArray(rule.use)) {
						rule.use = rule.use.map(e => {
							if ('object' === typeof e) {
								return convertToESBuild(e)
							}
							return e
						})
					} else {
						rule.use = convertToESBuild(rule.use)
					}
				}

				/**
				 *  Not sure why, but ESBuildMinifyPlugin makes the bundle larger.
				 *
				 * With Default Minimization:
				 * Main Bundle: 239 KB
				 * Largest Page: 122 KB
				 *
				 * With ESBuild Minification:
				 * Main Bundle: 664 KB
				 * Largest Page: 132 KB
				 *
				 * Probably a configuration error.
				 */

				// // Remove Default TerserPlugin
				// config.optimization.minimizer.shift();

				// // Add ESBuild Minify
				// config.optimization.minimizer.unshift(
				//   new ESBuildMinifyPlugin({
				//     target: 'es2017',
				//     minify: true,
				//   }),
				// );
			}

		if ('ESBUILD' === process.env.APP_MINIFY_MODE)
			if (!dev) {
				if ('ESBUILD' !== process.env.APP_BUILD_MODE)
					config.plugins.push(new ESBuildPlugin())
				// Remove Default TerserPlugin
				config.optimization.minimizer.shift()

				// Add ESBuild Minify
				config.optimization.minimizer.unshift(
					new ESBuildMinifyPlugin({
						target: 'es2017',
						minify: true,
					}),
				)
			}

		return config
	},
})
